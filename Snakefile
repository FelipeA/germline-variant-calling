
import pandas as pd
from snakemake.utils import min_version
import os

# Minimal version of snakemake
min_version('5.3.0')

# Load config and sample sheets #

configfile: 'config.yaml'

samples = pd.read_table(config['samples'], comment='#').set_index('sample', drop=False)
units = pd.read_table(config['units'], dtype=str, comment='#').set_index(['sample'], drop=False)

if 'run' in config['divided-by']:
    units = pd.read_table(
        config['units'], dtype=str, comment='#').set_index(['sample', 'run'], drop=False)

if 'lane' in config['divided-by']:
    units = pd.read_table(
        config['units'], dtype=str, comment='#').set_index(['sample', 'lane'], drop=False)

if 'run' in config['divided-by'] and 'lane' in config['divided-by']:
    units = pd.read_table(
        config['units'], dtype=str, comment='#').set_index(['sample', 'run', 'lane'], drop=False)


# Target rules #

rule all:
    input:
        # Bam file with Base Score Recalibration (BQSR)
        expand(
            'results/bqsr/{x}.{r}.bam',
            x=samples.index,r=config['reference']),
        # DeepVariant Single VCF
        expand(
            'results/deepvariant/{x}.{r}.vcf.gz',
            x=samples.index,r=config['reference']),
        # Snpsift Annotation VCF All Samples
        expand(
            'results/variantannot/1000G/all.{r}.snpsift.vcf.gz',
            r=config['reference']),
        # Stats bcftools
        expand(
            'results/stats/bcftools/{x}.{r}.stats',
            x=samples.index,r=config['reference']),

# Import rules #

include: 'rules/dumpsam.py'
include: 'rules/align.py'
include: 'rules/gatk.preprocess.py'
include: 'rules/deepvariant.py'
include: 'rules/variantannotation.py'