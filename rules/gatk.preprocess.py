# Filtering alignments by MAPQ value with SAMtools
rule quality_filter:
    input:
        bam = 'results/alignments/{x}.{r}.bam',
        bai = 'results/alignments/{x}.{r}.bai'
    output:
        bam = temp('results/tmp/quality_filter/{x}.{r}.bam'),
        bai = temp('results/tmp/quality_filter/{x}.{r}.bam.bai')
    threads: 2
    resources:
        mem = 1
    params:
        jobname = 'quality_filter.{x}.{r}',
        min_mapq = config['quality-filter'],
    container:
        "docker://faoaf/gatkimage:latest"
    shell:
        'samtools view -q{params.min_mapq} -b {input.bam} > {output.bam} '
        '&& samtools index {output.bam}'

# Mark duplicates process with GATK4 Picard MarkDuplicates Module 
rule markdup:
    input:
        bam = 'results/tmp/quality_filter/{x}.{r}.bam',
        bai = 'results/tmp/quality_filter/{x}.{r}.bam.bai'
    output:
        bam = temp('results/tmp/markdup/{x}.{r}.bam'),
        bai = temp('results/tmp/markdup/{x}.{r}.bai'),
        metrics = 'results/stats/markdup/{x}.metrics.{r}.txt'
    threads: 5
    resources:
        mem = 8
    log:
        'results/logs/markdup/{x}.{r}.log'
    params:
        jobname = 'markdup.{x}'
    container:
        "docker://faoaf/gatkimage:latest"
    shell:
        'gatk --java-options "-Xmx{resources.mem}G" MarkDuplicates -I {input.bam} -O {output.bam} '
        '--CREATE_INDEX true --REMOVE_DUPLICATES true --METRICS_FILE {output.metrics} > {log} 2>&1'

# First Step for Base Quality Score Recalibration (BQSR) process with GATK4 BaseRecalibrator Module
# BaseRecalibrator builds the model
rule baserecal:
    input:
        bam = 'results/tmp/markdup/{x}.{r}.bam',
        bai = 'results/tmp/markdup/{x}.{r}.bai'
    output:
        'results/stats/baserecalibrator/{x}.{r}.BaseRecalReport.grp'
    threads: 8
    resources:
        mem = 4
    log:
        'results/logs/variantcaller/baserecal.{x}.{r}.log'
    params:
        jobname = 'baserecal.{x}',
        genome = config[config['reference']]['genome'],
        dbsnp = config[config['reference']]['dbsnp']
    container:
        "docker://faoaf/gatkimage:latest"
    shell:
        'gatk --java-options "-Xmx{resources.mem}G" BaseRecalibrator '
        '-R {params.genome} -I {input.bam} -O {output} --known-sites {params.dbsnp} > {log} 2>&1'

# Second Step for BQSR process with GATK4 ApplyBQSR module
# ApplyBQSR adjusts the scores
rule applybqsr:
    input:
        bam = 'results/tmp/markdup/{x}.{r}.bam',
        bai = 'results/tmp/markdup/{x}.{r}.bai',
        table = 'results/stats/baserecalibrator/{x}.{r}.BaseRecalReport.grp'
    output:
        bam = 'results/bqsr/{x}.{r}.bam',
        bai = 'results/bqsr/{x}.{r}.bai'
    threads: 8
    resources:
        mem = 4
    log:
        'results/logs/variantcaller/applybqsr.{x}.{r}.log'
    params:
        jobname = 'baserecal.{x}',
        genome = config[config['reference']]['genome']
    container:
        "docker://faoaf/gatkimage:latest"
    shell:
        'gatk --java-options "-Xmx{resources.mem}G" ApplyBQSR '
        '-R {params.genome} -I {input.bam} -O {output.bam} '
        '--bqsr-recal-file {input.table} --create-output-bam-index true > {log} 2>&1'

