# Variant annotation with dbSNP using SnpSift
rule dbsnp_annotation:
    input: 
        'results/deepvariant/all.{r}.vcf.gz'
    output:
        'results/variantannot/dbsnp/all.{r}.snpsift.vcf.gz'
    resources:
        mem = 4
    params:
        jobname = 'snpsift.dbsnp.{r}',
        dbsnp = config[config['reference']]['dbsnp'],
    container:
        "docker://faoaf/gatkimage:latest"
    shell:
        'SnpSift -Xmx{resources.mem}G annotate {params.dbsnp} {input} | gzip > {output}'

# Variant annotation for population frequency using SnpSift
rule pop_annotation:
    input: 
        'results/variantannot/dbsnp/all.{r}.snpsift.vcf.gz'
    output:
        'results/variantannot/1000G/all.{r}.snpsift.vcf.gz'
    resources:
        mem = 4
    params:
        jobname = 'snpsift.1000G.{r}',
        ref = config[config['reference']]['1000G']
    container:
        "docker://faoaf/gatkimage:latest"
    shell:
        'SnpSift -Xmx{resources.mem}G annotate {params.ref} {input} | gzip > {output}'