def get_sam(wildcards):
    # divided by run
    if 'run' in units.index.names and 'lane' not in units.index.names:
        return expand('results/tmp/alignments/{sample}_{run}.sam.gz',
            run=units.index.levels[1], **wildcards)

    # divided by lane
    elif 'lane' in units.index.names and 'run' not in units.index.names:
        return expand('results/tmp/alignments/{sample}_{lane}.sam.gz',
            lane=units.index.levels[1], **wildcards)

    # divided by run and lane
    elif 'lane' in units.index.names and 'run' in units.index.names:
        return expand('results/tmp/alignments/{sample}_{run}_{lane}.sam.gz',
            run=units.index.levels[1], lane=units.index.levels[2], **wildcards)

    # divided by sample only
    else:
        return expand('results/tmp/alignments/{sample}.sam.gz', **wildcards)


def get_rginfo(wildcards):

    # divided by run
    if 'run' in units.index.names and 'lane' not in units.index.names:
        variable = '{sample}'.format(**wildcards).split('_')
        variable[0:2] = [''.join(variable[0 : 2])]
        sample, run = variable
        return '\'@RG\\tID:{s}.{r}\\tSM:{s}\\tPL:illumina\\tLB:{s}-{r}\\tPU:{s}.{n}\''.format(
            s=sample, r=run, n=run[-1])

    # divided by lane
    if 'lane' in units.index.names and 'run' not in units.index.names:
        variable = '{sample}'.format(**wildcards).split('_')
        variable[0:2] = [''.join(variable[0 : 2])]
        sample, lane = variable
        return '\'@RG\\tID:{s}.{e}\\tSM:{s}\\tPL:illumina\\tLB:{s}\\tPU:{s}.{n}\''.format(
            s=sample, e=lane, n=lane[-1])

    # divided by run and lane
    if 'lane' in units.index.names and 'run' in units.index.names:
        variable = '{sample}'.format(**wildcards).split('_')
        variable[0:3] = [''.join(variable[0 : 3])]
        sample, run, lane = variable
        return '\'@RG\\tID:{s}.{r}.{e}\\tSM:{s}\\tPL:illumina\\tLB:{s}-{r}\\tPU:{s}.{r}.{n}\''.format(
            s=sample, r=run, e=lane, n=lane[-1])

    # divided by sample only
    return '\'@RG\\tID:{sample}\\tSM:{sample}\\tPL:illumina\\tLB:{sample}\\tPU:unit1\''.format(
        **wildcards)

# Alignment Using BWA-MEM
rule bwa:
    input:
        p1 = 'results/tmp/fastq/{sample}_R1_001.fastq.gz',
        p2 = 'results/tmp/fastq/{sample}_R2_001.fastq.gz'
    output:
        sam = temp('results/tmp/alignments/{sample}.sam.gz')
    threads: 8
    resources:
        mem = 8
    log:
        'results/logs/alignments/bwa.{sample}.log'
    params:
        jobname = 'bwa.{sample}',
        rginfo = get_rginfo,
        genome = config[config['reference']]['genome'],
        avx = ''
    container:
        "docker://faoaf/gatkimage:latest"
    shell:
        'bwa mem -M -t {threads} -R {params.rginfo} {params.genome} {input.p1} '
        '{input.p2} 2>{log} | gzip > {output}'

# Merging units from BWA-MEM Using GATK4 MergeSamFiles module
rule merge_units_bwa:
    input: get_sam
    output:
        bam = 'results/alignments/{sample,\w+}.{r}.bam',
        bai = 'results/alignments/{sample,\w+}.{r}.bai'
    threads: 5
    resources:
        mem = 4
    log:
        'results/logs/alignments/picard.{sample}.{r}.log'
    params:
        jobname = 'merge_units_bwa.{sample}'
    container:
        "docker://faoaf/gatkimage:latest"
    shell:
        'gatk --java-options "-Xmx{resources.mem}G" MergeSamFiles '
        '-I {input[0]} -I {input[1]} -O {output.bam} -SO coordinate --CREATE_INDEX true > {log} 2>&1'


