# Creating temporary link with fastq files
rule link2fastq:
    input:
        'data/fastq/{sample}.fastq.gz'
    output:
        temp('results/tmp/fastq/{sample}.fastq.gz')
    threads: 1
    resources:
        mem = 0
    shell: 'ln -s ../../../{input} {output}'
