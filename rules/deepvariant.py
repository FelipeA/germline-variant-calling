# Germline variant-calling single-sample using DeepVariant
# VCF and gVCF as output
rule deepvariant:
    input:
        bam = 'results/tmp/markdup/{x}.{r}.bam',
        bai = 'results/tmp/markdup/{x}.{r}.bai'
    output:
        vcf = 'results/deepvariant/{x}.{r}.vcf.gz',
        gvcf = 'results/deepvariant/{x}.{r}.g.vcf.gz',
    log:
        'results/logs/deepvariant/deepvariant.{x}.{r}.log'
    params:
        jobname = 'deepvariant.{x}.{r}',
        genome = config[config['reference']]['genome'],
        tempdir = 'results/tmp/deepvariant',
        analysis = config['type-data']
    container:
        "docker://google/deepvariant:1.1.0"
    shell:  
        '/opt/deepvariant/bin/run_deepvariant --model_type=[params.analysis] --ref={params.genome} --reads={input.bam} --output_vcf={output.vcf} '
        '--output_gvcf {output.gvcf} --intermediate_results_dir {params.tempdir} --num_shards=8 > {log} 2>&1'

# Single-sample quality metrics
rule stats_bcftools:
    input:
        'results/deepvariant/{x}.{r}.vcf.gz'
    output:
        'results/stats/bcftools/{x}.{r}.stats'
    container:
        'docker://quay.io/mlin/glnexus:v1.2.7'
    shell:
        'bcftools stats -f PASS {input} > {output}'

# Single-sample variants (gVCF) merged with GLnexus following DeepVariant and GLnexus Best Practices
rule GLnexus:
    input: 
        vcfs = lambda wc: expand(
            'results/deepvariant/{x}.{r}.g.vcf.gz',
            x=samples.index, r=wc.r)
    output:
        'results/deepvariant/all.{r}.vcf.gz'
    params:
        jobname = 'glnexus',
        genome = config[config['reference']]['genome'],
        analysis = config['type-data']
    container:
        'docker://quay.io/mlin/glnexus:v1.2.7'
    shell:
        '/usr/local/bin/glnexus_cli --config DeepVariant[params.analysis] {input.vcfs} '
        '| bcftools view - | bgzip -c > {output}'