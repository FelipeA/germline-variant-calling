# Germline Variant Calling

[![Snakemake](https://img.shields.io/badge/snakemake-≥5.3-brightgreen.svg)](https://snakemake.bitbucket.io)

Germline Variant Calling Workflow utilizes [DeepVariants Best Practices](https://github.com/google/deepvariant/blob/r1.1/docs/trio-merge-case-study.md) for pre-processing of raw sequencing reads and for variant discovery of analysis-ready reads to deliver germline variants.

## Author

* Felipe de Azevedo Oliveira

## Usage

### Step 1: Configure Workflow

Configure the workflow according to your needs via editing the file `config.yaml`

#### Reference

You must set the reference that will be used in the pipeline.
This reference will be added to the output files created with this pipeline.

Open the file `config.yaml` and edit the line:

```yaml
# grch38
reference: grch38
```

#### Data Structure

Set the correct data structure of your samples.

```yaml
# [sample], [sample, run], [sample, lane], [sample, run, lane]
divided-by:
  - sample
  - run
  - lane
```

#### Samples

Create the sheets `samples.tsv` and `units.tsv` inside the directory `data` according to the example at `test-sample/` folder. FASTQ files are also expected as input at `data/fastq` folder.

### Step 2: Execute workflow

### Software needed

All you need to execute this workflow is to have Snakemake installed. Software needed by this workflow are pulled automatically from Docker and stored locally as singularity containers. 

#### Details

The `Dockerfile` stored in the `Dockerfiles/` folder was used to build the container `gatkimage` ([faoaf/gatkimage](https://hub.docker.com/repository/docker/faoaf/gatkimage) in Docker Hub) and is publicly available. It contains a base image for the latest version of the [Genome Analysis Toolkit (GATK)](https://hub.docker.com/r/broadinstitute/gatk) and software [BWA](http://bio-bwa.sourceforge.net), [SnpSift](https://pcingola.github.io/SnpEff/) and [SAMtools](http://samtools.sourceforge.net) are installed.

For germline variant-calling and merging of gVCFs [DeepVariant](https://github.com/google/deepvariant) and [GLnexus](https://github.com/dnanexus-rnd/GLnexus) were used. Both were pulled from their own Docker repositories.

### Execute workflow

Test your configuration by performing a dry-run via

```sh
snakemake -np
```

Execute the workflow via

```sh
snakemake --use-singularity
```

## Workflow

Workflow for test sample.

![Rule Graph](images/dag.svg)

## Structure of Directory

![Directory Structure](images/structure-directory.svg)



